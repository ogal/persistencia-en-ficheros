﻿### Proyecto Ficheros de texto.  


# Realiza un programa que dados dos ficheros de texto (uno de etiquetas (.txt) y otro de datos (.csv)) guarde un tercer fichero en formato XML.  

● El fichero de etiquetas es un fichero de texto plano con una etiqueta por línea.  
● El fichero de datos está en formato csv. En cada línea los valores de los campos  
correspondientes a cada etiqueta están separados por comas.  
● El programa debe generar el fichero XML correspondiente.  

Así por ejemplo, si disponemos del siguiente archivo de etiquetas:
~~~
Hola  
adeu
~~~
Y el siguiente fichero de datos:
~~~
1,2  
2,3  
3  
4,5,6,7  
~~~

Nota: observa que en la tercera línea falta un dato y que en la cuarta línea hay dos datos de
más.

Nuestro programa generaría el siguiente fichero XML:  
~~~
<doc>
    <elem nr="1">
        <hola>1</hola>
        <adeu>2</adeu>
    </elem>
    <elem nr="2">
        <hola>2</hola>
        <adeu>3</adeu>
    </elem>
    <elem nr="3">
        <hola>3</hola>
        <adeu></adeu>
    </elem>
    <elem nr="4">
        <hola>4</hola>
        <adeu>5</adeu>
        <altre>6</altre>
        <altre>7</altre>
    </elem>
</doc>  
~~~

Nota: “nr” es el número de línea en el fichero de datos


La implementación del programa solicitado debe:
1. Generar el archivo XML tal y como se ha indicado previamente.
2. Ser capaz de procesar ficheros de miles de líneas de datos.
3. Gestionar todos los errores posibles. Es decir, debe ser robusto.
Adicionalmente, el programa tiene que:
4. Generar un fichero de registro de todas las incidencias relevantes que se hayan
producido con el formato fecha+hora+”Mensaje de log”.
5. Solicitar los ficheros de entrada y el nombre del fichero de salida. Si el fichero de datos
de entrada es un directorio, la salida también debe serlo. En este caso, el programa
realizará un tratamiento masivo de los datos que consiste en:
a. El programa recorrerá todo el directorio.
b. Por cada fichero de datos que encuentre (extensión csv) debe generar un
archivo XML en el directorio de salida.
c. También generará un archivo de registro con todas las incidencias.

Así por ejemplo, si tenemos los parámetros de entrada listados a continuación:

i. Fichero/Directorio entrada: “./entrada” con 10 archivos (3 con extensión
.csv)  
ii. Archivo de etiquetas: “./eti.txt”  
iii. Fichero/Directorio salida: “./salida”, si se indica el fichero de etiquetas
“etiquetas.txt”  

el programa generará en “./salida” tres archivos XML (uno por cada .csv con las etiquetas de
“eti.txt”) y un archivo de registro (log) con todos los mensajes de incidencias que se consideren
oportunos.