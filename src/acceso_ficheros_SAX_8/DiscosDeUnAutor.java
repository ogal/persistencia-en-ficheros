package acceso_ficheros_SAX_8;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;

/** M6 - UF1
 * Crea un programa que devuelva el n�mero de discos registrados en
 * �discoteca.xml� de un determinado autor que le pasamos por consola. 
 * Si el autor carece de discos en el archivo, el programa devolver� un mensaje del
 * estilo: �El autor <xxxxxx> no aparece en el archivo.
 * */

public class DiscosDeUnAutor {

public static void main(String[] args) throws FileNotFoundException, IOException, SAXException {
		
		XMLReader procesadorXML = XMLReaderFactory.createXMLReader();
		
		Scanner teclado = new Scanner(System.in);
		System.out.println("Los discos de que autor est� buscando?");
		
		ColeccionDiscos gestor = new ColeccionDiscos(teclado.nextLine());
		procesadorXML.setContentHandler(gestor);
		
		InputSource fileXML = new InputSource ("discoteca.xml");
		procesadorXML.parse(fileXML);
		
		teclado.close();
		
	}//main

}//end class main


class ColeccionDiscos extends DefaultHandler {
	
	String autor;
	static int etiqueta = 0;
	static ArrayList<String> discosEncontrados = new ArrayList<String>();
	
	public ColeccionDiscos(String autor){
		super();
		this.autor = autor;
	}
	public void startDocument(){
		System.out.println("Comienzo del documento XML");
	}
	public void endDocument(){
		System.out.println("Final del documento XML");
		
		if (discosEncontrados.isEmpty()) {
			
			System.out.println("Parece que no hemos encontrado ningun disco de este autor: " + autor + ".");
			
		} else {
			System.out.println("Se han encontrado los siguientes discos: ");
			for(String disco : discosEncontrados) {
				System.out.print(disco + " ");
			}
		}
	}
	
	public void startElement (String uri, String nombre, String nombreC, Attributes atts) {
		System.out.printf("\tPrincipio Elemento: %s %n", nombre);
		
		if(nombre.toLowerCase().equals("title")) {
			etiqueta = 1;		
		}else if(nombre.toLowerCase().equals("artist")){
			etiqueta = 2;
		}else {
			etiqueta = 0;
		}
	}
	
	public void endElement (String uri, String nombre, String nombreC){
		System.out.println("Fin Elemento: " + nombre + "  " + nombreC);
	}
	
	public void characters(char[] ch, int inicio, int longitud) throws SAXException {
		String car = new String (ch, inicio, longitud);
		car = car.replaceAll("[\t\n]","");
		System.out.printf("\tCaracteres: %s %n", car);
		
		if (etiqueta == 1) { // Los titulos se guardan todos por defecto
			
			discosEncontrados.add(car.toLowerCase());
			
		} else if(etiqueta == 2) { // Si la etiqueta es el artista comparamos con lo que ha introducido el usuario
			
			if(!car.toLowerCase().equals(autor.toLowerCase().trim())) {
				
				discosEncontrados.remove(discosEncontrados.size()-1);
				
			}
		}
	}
}
