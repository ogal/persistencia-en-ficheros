package acceso_ficheros_SAX_8;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * Ejercicio del canal TechKrowd
 * */

public class VersionesSAX {

	public static void main(String[] args){
		
		// Necesitamos una factoria para poder instanciar el objeto SAXParser
		SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
		
		try {
			// Objeto que vamos a utilizar para leer nuestro doc XML
			SAXParser saxParser = saxParserFactory.newSAXParser();
			
			// Declaramos el archivo y el manejador que utilizaremos
			File archivo = new File("versiones.xml");
			
			ManejadorContenido manejadorContenido = new ManejadorContenido();
			
			saxParser.parse(archivo, manejadorContenido);
			
			ArrayList<Version> versiones = manejadorContenido.getVersiones();
			
			for( Version version : versiones ) {
				
				System.out.println(version);
			}
			
		} catch (ParserConfigurationException | SAXException | IOException e) {
			
			e.printStackTrace();
		}
		
		

	}

}

/** Tenemos en cuenta, etiquetas de apertura, de cierre y contenido, para poder leer el archivo XML */
/** Dentro de la clase hacemos click derecho > Source > Override/Implement Methods > Seleccionamos aquellos que queramos */
class ManejadorContenido extends DefaultHandler {
	
	ArrayList<Version> versiones = new ArrayList<>();
	private Version version;
	private StringBuilder buffer = new StringBuilder();
	
	public ArrayList<Version> getVersiones() {
		return versiones;
	}

	/** Este metodo se ejecutará cuando encuentre una etiqueta XML de apertura */
	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		
		switch (qName) {
		case "versiones":
			
			break;
		case "version": // etiqueta de apertura 
			version = new Version();
			versiones.add(version);
			version.setNumeroVersion(Double.parseDouble(attributes.getValue("numero")));
			break;
		case "nombre":
		case "api":
			buffer.delete(0, buffer.length()); // vacia el buffer de texto para que en el metodo characters se pueda almacenar 
			break;

		default:
			break;
		}
	}

	/** Este metodo se nos dispara cuando el parser se topa con una etiqueta de cierre */
	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		switch (qName) {
		case "nombre":
			version.setNombreVersion(buffer.toString()); // cuando se topa con la etiqueta de cierre carga el texto en el objeto
			break;
		case "api":
			version.setApi(Integer.parseInt(buffer.toString()));
			break;
		// Tanto version como versiones, no nos aportan ninguna utilidad al programa, se podrian eliminar
		case "version":
	
			break;
		case "versiones":
	
			break;
		default:
			break;
		}
	}

	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		// Toma el buffer que hemos vaciado y cargamos el texto 
		buffer.append(ch, start, length);
	}
	
}

class Version {
	
	private double numeroVersion;
	private String nombreVersion;
	private int api;
	
	public Version() {
		
	}

	public double getNumeroVersion() {
		return numeroVersion;
	}

	public void setNumeroVersion(double numeroVersion) {
		this.numeroVersion = numeroVersion;
	}

	public String getNombreVersion() {
		return nombreVersion;
	}

	public void setNombreVersion(String nombreVersion) {
		this.nombreVersion = nombreVersion;
	}

	public int getApi() {
		return api;
	}

	public void setApi(int api) {
		this.api = api;
	}

	@Override
	public String toString() {
		return "Version [numeroVersion=" + numeroVersion + ", nombreVersion=" + nombreVersion + ", api=" + api + "]";
	}
		
}





