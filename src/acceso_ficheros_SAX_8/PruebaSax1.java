package acceso_ficheros_SAX_8;

import java.io.*;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;
/** 
 * M6 - UF1
 * Ejercicio por defecto tal y como nos lo presentan en el PDF de los apuntes
 * Enlace a la api, buscar la clase DefaultHandler --> https://docs.oracle.com/javase/7/docs/api/
 * Clases a utilizar: 
 * - Los helpers DefaultHandler y XMLReaderFactory
 * - La clase InputSource
 * - Las interfaces 
 * - ContentHandler → recibe las notificaciones de los eventos que ocurren en el documento
 * - DTDHandler → recoge eventos relacionados con la DTD (Definición de Tipo de Documento)
 * - ErrorHandler → define métodos de tratamientos de errores
 * - EntityResolver
 * */
public class PruebaSax1 {

	public static void main(String[] args) throws FileNotFoundException, IOException, SAXException {
			
		// 1. Se crea objeto procesador
		XMLReader procesadorXML = XMLReaderFactory.createXMLReader();
			
		/* 2. Mediante setContentHandler establecemos que la clase que gestiona los eventos provocados por la
		lectura del XML será GestionContenido */
		GestionContenido gestor = new GestionContenido();
		procesadorXML.setContentHandler(gestor);
			
		/* 3. Se define el fichero que se va leer mediante InputSource y se procesa el documento XML mediante el
		método parse() de XMLReader */
		InputSource fileXML = new InputSource ("discoteca.xml");
		procesadorXML.parse(fileXML);
		
	}//main
}//end class main

/* GestionContenido es un manejador, esto es, un objeto que define como es la estructura del XML que voy a leer y 
 * como interpretar a estructura del fichero.
 *  */
class GestionContenido extends DefaultHandler {
	
	public GestionContenido(){
		super();
	}
	
	public void startDocument(){
		System.out.println("Comienzo del documento XML");
	}
	
	public void endDocument(){
		System.out.println("Final del documento XML");
	}
	
	public void startElement (String uri, String nombre, String nombreC, Attributes atts) {
		System.out.printf("\tPrincipio Elemento: %s %n", nombre);
	}
	
	public void endElement (String uri, String nombre, String nombreC){
		System.out.printf("\tFin Elemento: %s %n",nombre);
	}
	
	public void characters(char[] ch, int inicio, int longitud) throws SAXException {
		String car = new String (ch, inicio, longitud);
		car = car.replaceAll("[\t\n]","");
		System.out.printf("\tCaracteres: %s %n", car);
	}
}
