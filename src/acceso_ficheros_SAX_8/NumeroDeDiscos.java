package acceso_ficheros_SAX_8;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;

/**
 * M6 - UF1
 * Crea un programa que lea el archivo �discoteca.xml� (adjunto en el moodle) y
 * determine el n�mero de discos listados en el archivo. 
 * AYUDA: contabilizando el n�mero de t�tulos.
 * */

public class NumeroDeDiscos {
	
	public static void main(String[] args) throws FileNotFoundException, IOException, SAXException {
		
		XMLReader procesadorXML = XMLReaderFactory.createXMLReader();
		
		Discos discos = new Discos();
		procesadorXML.setContentHandler(discos);
		
		InputSource fileXML = new InputSource ("discoteca.xml");
		procesadorXML.parse(fileXML);
		
		System.out.println("\nHay un total de: " + Discos.numDiscos + " discos.");
		
	}//main

}//end class main


class Discos extends DefaultHandler {
	
	static int numDiscos = 0;
	
	public Discos(){
		super();
	}
	
	public void startDocument(){
		System.out.println("Comienzo del documento XML");
	}
	
	public void endDocument(){
		System.out.println("Final del documento XML");
	}
	
	public void startElement (String uri, String nombre, String nombreC, Attributes atts) {
		System.out.print("\tPrincipio Elemento nombre: " + nombre + " y " + nombreC);
		
		if(nombre.equals("CD")) {
			numDiscos++;
		}
	}
	
	public void endElement (String uri, String nombre, String nombreC){
		System.out.printf("\tFin Elemento: %s %n",nombre);
	}
	
	public void characters(char[] ch, int inicio, int longitud) throws SAXException {
		String car = new String (ch, inicio, longitud);
		car = car.replaceAll("[\t\n]","");
		System.out.printf("\tCaracteres: %s %n", car);
	}
}
