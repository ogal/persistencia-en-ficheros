package acceso_ficherosXML_7;

import java.io.File;
import javax.xml.parsers.*;
import org.w3c.dom.Document;
// import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class LecturaFicheroXML {

	public static void leeElFicheroXML() {
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		
		try {
			
			DocumentBuilder builder = factory.newDocumentBuilder();
			// Parse the content of the given file as an XML document and return a new DOM Document object.
			Document document = builder.parse(new File ("Empleados.xml"));
			
			System.out.println ("Elemento ra�z : " + document.getDocumentElement().getNodeName());
			
			NodeList empleados = document.getElementsByTagName("empleado");
			System.out.println ("Nodos empleado a recorrer: " + empleados.getLength());
			
			System.out.println("-----------------------------------------------------");
			
			for (int i = 0; i < empleados.getLength(); i++) {
				
				Node empleado = empleados.item(i);
				
				System.out.println(
						"\nEmpleado: " + empleado.getChildNodes().item(0).getTextContent() + "\n"
						+ "Nombre: " + empleado.getChildNodes().item(1).getTextContent() + "\n"
						+ "Departamento: " + empleado.getChildNodes().item(2).getTextContent() + "\n"
						+ "Salario: " + empleado.getChildNodes().item(3).getTextContent());
				
				// The node is an Element.
//				if (empleado.getNodeType() == Node.ELEMENT_NODE){
//					
//					Element elemento = (Element) empleado;
//					
//					System.out.printf("Identificador = %s %n",
//							elemento.getElementsByTagName("id").item(0).getTextContent());
//					System.out.printf(" * nombre = %s %n",
//							elemento.getElementsByTagName("apellido").item(0).getTextContent());
//					System.out.printf(" * departamento = %s %n",
//							elemento.getElementsByTagName("departamento").item(0).getTextContent());
//					System.out.printf(" * salario = %s %n",
//							elemento.getElementsByTagName("salario").item(0).getTextContent());
//				}
			}
			
		}catch (Exception e) {
			
			e.printStackTrace();
		}
		
	}
	
	
	public static void main(String[] args) {
		
		leeElFicheroXML();

	}

}
