package acceso_ficherosXML_7;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

public class CrearXMLEmpleado {
	
	static void CrearElemento (String datoEmpleado, String valor, Element raiz, Document document) {
		Element elem = document.createElement (datoEmpleado);
		Text text = document.createTextNode(valor);
		raiz.appendChild (elem);
		elem.appendChild (text);
	}

	public static void main(String[] args) {
		
		File fichero = new File ("Empleados.txt");
		FileReader lector = null;
		BufferedReader file = null; 
		String[] partes1;
		String linea;
		
		ArrayList<String> apellidos = new ArrayList<String>();
		ArrayList<Integer> ids = new ArrayList<Integer>();
		ArrayList<Integer> deps = new ArrayList<Integer>();
		ArrayList<Double> salarios = new ArrayList<Double>();
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		
		try {
			
			lector = new FileReader(fichero);
			file = new BufferedReader(lector);
			
			builder = factory.newDocumentBuilder();
			
			DOMImplementation implementation = builder.getDOMImplementation();
			Document document = implementation.createDocument (null,"Empleados", null);
			document.setXmlVersion("1.0");
			
				while((linea = file.readLine()) != null) {
					
			       partes1 = linea.split(":");
			       
				   ids.add(Integer.parseInt(partes1[0]));
				   apellidos.add(partes1[1]);
				   deps.add(Integer.parseInt(partes1[2]));
				   salarios.add(Double.parseDouble(partes1[3])); 
				}
				
			 	for(int i = 0; i < apellidos.size(); i++) {
			 		
					Element raiz = document.createElement ("empleado");
					document.getDocumentElement().appendChild(raiz);
					
					CrearElemento ("id", Integer.toString(ids.get(i)), raiz, document);
					CrearElemento ("apellido",apellidos.get(i).trim(), raiz, document);
					CrearElemento ("dep", Integer.toString(deps.get(i)), raiz, document);
					CrearElemento ("salario", Double.toString(salarios.get(i)),raiz, document);
				}
				
			Source source = new DOMSource (document);
			Result result = new StreamResult (new java.io.File ("Empleados2.xml"));
			Transformer transformer = TransformerFactory.newInstance().newTransformer();
			transformer.transform (source, result);
			
			file.close();
			
		} catch (Exception e ) { System.err.println ("Error: " + e);}
		
	}

}
