package acceso_ficherosXML_7;

import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import java.io.*;
/** A continuaci�n vamos a estudiar un programa Java que va a crear un fichero XML a partir del fichero
 * aleatorio �AleatorioEmpleado.dat� que ya hab�amos creado anteriormente. El fichero
 * �AleatorioEmpleado.dat� contiene un registro por cada empleado en el que se utilizan 36 bytes para
 * almacenar el id del empleado, su apellido, su departamento y su salario. 
*/
public class CrearEmpleadoXml {
	
	public static void main(String args[]) throws IOException {
		
		File fichero = new File("place.txt");
		RandomAccessFile file = new RandomAccessFile(fichero, "r");
		int id, dep, posicion = 0;
		Double salario;
		char apellido[] = new char[10], aux;
		
		/* Aqu� estamos creando una instancia de DocumentBuilderFactory para construir el parser. Se debe
		encerrar entre try-catch porque se puede producir la excepci�n ParserConfigurationException*/
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		
		try {
			DocumentBuilder builder = factory.newDocumentBuilder();
			
			/*Creamos un documento vac�o de nombre document con el nodo ra�z de nombre Empleados y
			asignamos la versi�n XML. La interfaz DOMImplementation permite crear objetos Document con nodo
			ra�z*/
			DOMImplementation implementation = builder.getDOMImplementation();
			Document document = implementation.createDocument(null, "Empleados", null);

			document.setXmlVersion("1.0");
			for (;;) {

				file.seek(posicion);
				id = file.readInt();

				for (int i = 0; i < apellido.length; i++) {
					aux = file.readChar();
					apellido[i] = aux;

				}
				String apellidos = new String(apellido);
				dep = file.readInt();
				salario = file.readDouble();
				
				/* A continuaci�n, por cada registro en el fichero AleatorioEmpleado.dat, debemos crear un nodo
				empleado que, a su vez, tendr� 4 nodos-hijo. Cada nodo hijo tendr� su valor (por ejemplo: 1,
				FERNANDEZ, 10, 1000.45). Para crear un elemento usamos el m�todo createElement (String) llevando
				como par�metro el nombre que se pone entre las etiquetas menor que y mayor que. */
				if (id > 0) {
					Element raiz = document.createElement("empleado");
					document.getDocumentElement().appendChild(raiz);
					CrearElemento("id", Integer.toString(id), raiz, document);
					CrearElemento("apellido", apellidos.trim(), raiz, document);
					CrearElemento("dep", Integer.toString(dep), raiz, document);
					CrearElemento("salario", Double.toString(salario), raiz, document);

				}
				posicion = posicion + 36;
				if (file.getFilePointer() == file.length())
					break;
			}
			/*La especificaci�n DOM no define ning�n mecanismo para generar un fichero XML a partir de un �rbol
			DOM. Para eso usaremos el paquete javax.xml.transform que permite especificar una fuente y un
			resultado. La fuente y el resultado pueden ser ficheros, flujos de datos o nodos DOM entre otros.
			
			Primero crearemos la fuente (Source) XML a partir del documento (document); despu�s se crea el
			resultado (Result) en el fichero Empleados.xml. A continuaci�n, se obtiene un TransformerFactory y se
			realiza la transformaci�n del documento al fichero */
			Source source = new DOMSource(document);
			Result result = new StreamResult(new java.io.File("Empleados.xml"));
			Transformer transformer = TransformerFactory.newInstance().newTransformer();
			transformer.transform(source, result);
		} catch (Exception e) {
			System.err.println("Error: " + e);
		}
		file.close();
	}//main clousure

	/* Este m�todo, como hemos visto anteriormente, genera los nodos-hijo de cada nodo <Empleado>.
	Este m�todo es llamado 4 veces por cada Empleado. Genera los nodos <id>, <apellido>,<dep> y
	<salario>. Primero genera el elemento (Element), despu�s el texto o valor (Text) y los asocia con la raiz
	o nodo padre */
	static void CrearElemento(String datoEmpleado, String valor, Element raiz, Document document) {
		Element elem = document.createElement(datoEmpleado);
		Text text = document.createTextNode(valor);
		raiz.appendChild(elem);
		elem.appendChild(text);
	}
}//class clousure
