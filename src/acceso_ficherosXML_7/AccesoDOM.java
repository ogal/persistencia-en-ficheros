package acceso_ficherosXML_7;

import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class AccesoDOM {

	/** Es llamado cada vez que queremos crear un nuevo nodo de tipo hijo */
	static void CrearElemento (String nombreEtiqueta, String valor, Element raiz, Document document) {
		
		Element elem = document.createElement(nombreEtiqueta);
		Text text = document.createTextNode(valor);
		raiz.appendChild (elem);
		elem.appendChild (text);
	
	}
	
	public static void main(String[] args) {
		
		Scanner lectura = null;
		int id, dep;
		Double salario;
		String apellido;
		
		ArrayList<String> empleados = new ArrayList<>();
		
		try {
			lectura = new Scanner(new File("Empleados.txt"));
			
			// Tomamos cada una de las l�neas del fichero en un ArrayList
			while (lectura.hasNextLine()) {
				
				empleados.add(lectura.nextLine());
			}
			
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			
			DocumentBuilder builder = factory.newDocumentBuilder();
			DOMImplementation implementation = builder.getDOMImplementation();
			Document document = implementation.createDocument (null,"Empleados", null);
			document.setXmlVersion("1.0");
			
			for (int i = 0; i < empleados.size() ;i++){
	
				// Tomamos cada una de las l�neas en un array de palabras
				String[] empleado = empleados.get(i).split(":");
	
				id = Integer.parseInt(empleado[0]);
				apellido = empleado[1];
				dep = Integer.parseInt(empleado[2]);
				salario = Double.parseDouble(empleado[3]);
				
				if ( id > 0 ) {
					
					Element raiz = document.createElement ("empleado");
					document.getDocumentElement().appendChild(raiz);
					CrearElemento ("id", Integer.toString(id), raiz, document);
					CrearElemento ("apellido", apellido.trim(), raiz, document);
					CrearElemento ("departamento", Integer.toString(dep), raiz, document);
					CrearElemento ("salario", Double.toString(salario), raiz, document);
				}
			}
			
			Source source = new DOMSource (document);
			Result result = new StreamResult (new java.io.File ("Empleados.xml"));
			Transformer transformer = TransformerFactory.newInstance().newTransformer();
			transformer.transform (source, result);
			
		} catch (Exception e ) { 
			System.err.println ("Error: " + e);
		
		}finally {
			
			System.out.println("El programa ha finalizado.");
			lectura.close();
		}
		
	}

}
