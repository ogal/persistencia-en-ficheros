package acceso_aleatorio;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * Ejemplo de programa que encontramos en el PDF
 * */
public class EjemploBase {
	
	public static void main(String[] args) throws IOException{
		
		File fichero = new File ("AleatorioEmpleado.txt");
		RandomAccessFile acceso = new RandomAccessFile (fichero , "rw");//Declaramos el fichero read/write
		
		//Datos empleados
		String[] apellido = {"Fern�ndez", "Gil", "L�pez", "Ramos"};
		int[] departamento = {10, 20, 10, 10};
		Double[] salario = {1000.45, 2400.60, 3000.0, 1500.56};
		
		StringBuffer buffer = new StringBuffer(); //Buffer para almacenar apellido
		
		int n = apellido.length; //N�mero de elementos en el array
		
		for (int i = 0; i < n; i++) { //Recorro los arrays
			
			acceso.writeInt (i+1);
			
			buffer.insert(i - apellido[i].indexOf(i) , apellido[i]);
			buffer.setLength(10); // Fijo en 10 caracteres la longitud del apellido
			
			acceso.writeChars (buffer.toString());
			acceso.writeInt(departamento[i]);
			acceso.writeDouble (salario[i]);
		}
		
		acceso.close(); // No olvidar cerrar el fichero
		
	}

}
