package gestion_ficheros_2;

import java.io.File;
import java.util.Scanner;

/** PDF2 
 * Ejemplo 1 - Muestra la lista de ficheros en el directorio actual
 * */
public class VerDir {
	
	static Scanner teclado = new Scanner(System.in);
	
	public static void main (String[] args) {
		
		// C:\Users\oscar\Documents
		System.out.println("Introduce una ruta absoluta para listar su contenido: \n");
		String ruta = teclado.nextLine(); // directorio objetivo
		
		try {
			File directorio = new File(ruta);
			String[] archivos = directorio.list();
			
			System.out.printf("Ficheros en el directorio actual: %d %n", archivos.length);
			
			for (int i=0; i < archivos.length; i++){
				
				File f2 = new File(directorio, archivos[i]);
				System.out.printf("Nombre: %s, es fichero?: %b, es directorio?:%b %n", archivos[i],
					f2.isFile(), f2.isDirectory());
			}
			
		}catch (NullPointerException e) {
			
			System.out.println("Lo sentimos, parece que el directorio especificado no existe. \n Intentelo de nuevo.");
			
		}finally {
			
			System.out.println("Programa finalizado.");
		}
	}
}
