package gestion_ficheros_2;

import java.io.File;
import java.io.IOException;

/** PDF2 
 * Ejemplo 2 - Creaci�n/eliminaci�n de archivos y carpetas
 * */
public class CrearDir {
	
	public static void main(String[] args){
		
	
		File directorio = new File("NuevoDir");
		File fichero1 = new File(directorio, "Fichero1.txt");
		File fichero2 = new File (directorio, "Fichero2.txt");
		
		directorio.mkdir();
		
		try {
			if (!fichero1.exists()) {
				fichero1.createNewFile();
				System.out.println("Fichero1 creado correctamente");
			} else {
				System.out.println("El fichero 1 ya existe!");
			}
			
			if (!fichero2.exists()) {
				fichero2.createNewFile();
				System.out.println("Fichero2 creado correctamente");
			} else {
				System.out.println("El fichero 2 ya existe!");
			} 
			
		} catch (IOException e) {
				System.out.println("Ha ocurrido un error durante la creaci�n de los ficheros.");
				
		} finally {
			System.out.println("Ficheros creados.");
		}
			
		// Renombramos el fichero1 
		fichero1.renameTo( new File(directorio, "Fichero1Nuevo.txt"));
		
		//Entramos en el directorio nuevo y entramos un tercer fichero
		try {
			File fichero3 = new File ("NuevoDir/Fichero3.txt");
			fichero3.createNewFile();
			System.out.println("Fichero 3 creado correctamente.");
			
		} catch (IOException ioe) {
			System.out.println("El fichero3 no ha sido creado.");
			
		} finally {
			
			File archivos[] = directorio.listFiles();
			System.out.println("Se han creado los archivos: ");
			
			for (int i = 0; i < archivos.length; i++) {
				
				System.out.println("Archivo " + archivos[i].getName());
				
			}
		}
		
	}
}
