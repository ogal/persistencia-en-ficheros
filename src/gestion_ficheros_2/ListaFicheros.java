package gestion_ficheros_2;

import java.io.File;
import java.util.Scanner;

/** 
 * Realiza un programa Java que utilice el m�todo listFiles() para mostrar la lista
 * de ficheros de un directorio que se pasar� al programa desde los argumentos del main. 
 *  */
public class ListaFicheros {
	
	// C:\Users\oscar\Documents
	static Scanner teclado = new Scanner(System.in);
	
	public static void listaElContenidoDelDirectorio() {
		
//		System.out.println("Introduce una ruta absoluta para listar su contenido: \n");
//		String ruta = teclado.nextLine(); // directorio objetivo
		
		try {
			File directorio = new File("C:\\Users\\oscar\\Documents");
			
			if(directorio.isDirectory()) {
				File[] archivos = directorio.listFiles();
				
				System.out.println("Ficheros en el directorio actual: " + archivos.length);
				
				for (File archivo : archivos){
					
					System.out.println(" - " + archivo.getName());
					
				}
			}else {
				System.out.println("No nos ha indicado un directorio sino un archivo.");
			}
			
			
		}catch (NullPointerException e) {
			
			System.out.println("Lo sentimos, parece que el directorio especificado no existe. \n Intentelo de nuevo.");
			
		}finally {
			
			System.out.println("Programa finalizado.");
		}
		
	}
	
	public static void informacionDelFichero(String ruta) {
		
		File fichero = new File(ruta);
		
		if(fichero.exists()) {
			System.out.println(fichero.getPath() + "  " + fichero.getAbsolutePath() + "  " + fichero.canRead() + "  " +
					fichero.canWrite() + "  " + fichero.length()
		);
		}else {
			System.out.println("No se ha podido encontrar el fichero");
		}
		
	}

	public static void main(String[] args) {
		
		listaElContenidoDelDirectorio();
				
		

	}

}
