package gestion_ficheros_2;

import java.io.File;
import java.io.IOException;

/**Programa PDF2 Gesti�n de ficheros
 * Elimina el directorio junto a los archivos que contiene
 * */
public class EliminarDir {

	public static void main(String[] args) {
		
		File directorio = new File("NuevoDir");
		File fichero1 = new File(directorio, "Fichero1.txt");
		File fichero2 = new File (directorio, "Fichero2.txt");
		
		directorio.mkdir();
		
		try {
			if (!fichero1.exists()) {
				fichero1.createNewFile();
				System.out.println("El fichero 1 ya existe!");
			} else {
				System.out.println("No se ha podido crear el Fichero1");
			}
			
			if (!fichero2.exists()) {
				fichero2.createNewFile();
				System.out.println("Fichero2 creado correctamente");
			}else {
				System.out.println("El fichero 2 ya existe!");
			} 
			
		} catch (IOException e) {
				System.out.println("Ha ocurrido un error durante la creaci�n de los ficheros.");
				
		} finally {
			System.out.println("Ficheros creados.");
		}
			
		// Renombramos el fichero1 
		fichero1.renameTo( new File(directorio, "Fichero1Nuevo.txt"));
		
		//Entramos en el directorio nuevo y entramos un tercer fichero
		try {
			File fichero3 = new File ("NuevoDir/Fichero3.txt");
			fichero3.createNewFile();
			
		} catch (IOException ioe) {
			System.out.println("");
			
		} finally {
			
			File archivos[] = directorio.listFiles();
			
			for (int i = 0; i < archivos.length; i++) {
				
				archivos[i].delete();
				
				if (!archivos[i].exists()) {
					System.out.println(archivos[i].getName() + " Ha sido eliminado.");
				}
			}
			
			directorio.delete();
			
			System.out.println("El directorio " + directorio.getName() + " ha sido eliminado.");
		}
		
	}
}
