package entregauf1;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

//import javax.swing.JFileChooser;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

public class Main {
	
	//1. Create DOCUMENTO RESULTADO en XML que aunar� los archivos de etiquetas y contenidos
	static public Document generaElDocumento() throws IOException{
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		Document document = null;
		
		try {
			builder = factory.newDocumentBuilder();
			DOMImplementation implementation = builder.getDOMImplementation();
			document = implementation.createDocument (null, "documento", null); //documento es la etiqueta XML raiz
			document.setXmlVersion("1.0");
			
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
		
		return document;
	}
	
	//2. READ CONTENT LINES
	static public ArrayList<String> leelineas() throws IOException{
		
		//Caja de selecci�n del archivo1 que ser� el archivo de contenidos
//		JFileChooser archivo1 = new JFileChooser();
//		archivo1.setDialogTitle("Escoja el archivo de contenidos en csv");
//		archivo1.showOpenDialog(null);
//		
//		File contenidos = archivo1.getSelectedFile();
		
		//Con el archivo tomado, hacemos una instancia de un objeto lector:
//		Scanner lector = new Scanner(new File(contenidos.getAbsolutePath()));
		Scanner lector = new Scanner(new File("contenidos.csv"));// COMENTAR ESTA LINEA SI SE DESCOMENTA LO ANTERIOR
		ArrayList<String> lineas = new ArrayList<String>();
		
		while(lector.hasNextLine()) {
			lineas.add(lector.nextLine());
		}

		lector.close();	
		return lineas;
	
	}
	
	/** Es llamado cada vez que queremos crear un nuevo nodo de tipo hijo */
	static void CrearElemento (String nombreEtiqueta, String valor, Element raiz, Document document) {
		
		Element elem = document.createElement(nombreEtiqueta);
		Text text = document.createTextNode(valor);
		raiz.appendChild (elem);
		elem.appendChild (text);
	
	}
	
	static void generaLosNodosHijo(ArrayList<String> contenidos, Document document) {
		
		File etiquetas = new File("etiquetas.txt");
		Scanner lectura = null;
		ArrayList<String> nodos = new ArrayList<>();
		String[] nodo = null;
		
		try {
			lectura = new Scanner(etiquetas);
			
			while (lectura.hasNextLine()) {
				nodos.add(lectura.nextLine());	
			}
			
			nodo = new String[nodos.size()];
			
			for(int i = 0; i < nodo.length; i++) {
				
				nodo[i] = nodos.get(i);
			}
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		for (int i = 0; i < contenidos.size() ;i++){ // nodos.size = 2, es el numero de etiquetas
			
			// Tomamos cada una de las l�neas en un array de palabras
			String[] pais = contenidos.get(i).split(",");
				
			Element raiz = document.createElement ("pais");
			document.getDocumentElement().appendChild(raiz);
				
			for (int k = 0; k < nodo.length; k++) {
				
				CrearElemento (nodo[k], pais[k].trim(), raiz, document);

			}
		}
		lectura.close();
	}
		
	
	public static void main(String[] args) throws IOException, TransformerException {
		
		// Generamos el documento
		Document documento = generaElDocumento();
		
		//Leemos cada una de las lineas de los contenidos y las etiquetas
		ArrayList<String> paises =  leelineas();
		
		generaLosNodosHijo(paises, documento);
		
		Source source = new DOMSource (documento);
		Result result = new StreamResult (new java.io.File ("documentoPaises.xml"));
		Transformer transformer;
		
		try {
			transformer = TransformerFactory.newInstance().newTransformer();
			transformer.transform (source, result);
			
		} catch (TransformerConfigurationException | TransformerFactoryConfigurationError e) {
	
			e.printStackTrace();
			
		}finally {
			System.out.println("\nPROGRAMA FINALIZADO");
		}
		
	}

}
