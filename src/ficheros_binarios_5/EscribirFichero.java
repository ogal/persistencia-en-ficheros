package ficheros_binarios_5;

import java.io.FileWriter;
import java.io.IOException;

/**
 * Documento de testeo.
 * */
public class EscribirFichero {

	public static void main(String[] args) {
		
		Escribiendo escribiendo = new Escribiendo();
		
		escribiendo.escribir();

	}

}

class Escribiendo {
	
	public void escribir() {
		
		String frase = "Frase de testeo para escribir en un fichero. Dos veces.";
		
		try {
			
			//Como ya hemos ejecutado el programa antes, el archivo ya existe, por tanto podemo a�adir un true como segundo parametro
			FileWriter escritura = new FileWriter("C:/Users/oscar/eclipse-workspace/m6/mifichero.txt", true);
			
			for(int i = 0; i < frase.length(); i++) {
				
				escritura.write(frase.charAt(i));
				
			}
			
			escritura.close();
			
		} catch (IOException e) {
			
			e.printStackTrace();
			System.out.println("No se ha encontrado la ruta al archivo.");
			
		}finally {
			
			System.out.println("Programa terminado.");
		}
		
	}
	
	
	
	
}
