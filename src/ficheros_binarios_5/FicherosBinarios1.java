package ficheros_binarios_5;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
/**
 * PDF 5 - Ficheros binarios 1 EJERCICIO 2
 * Utilizaremos los metodos: writeUTF(String str) y para la edad: writeInt(int v) 
 * FileInputStream (Lectura) y FileOutputStream (Escritura).
 * */

public class FicherosBinarios1 {

	public static void main(String[] args) {
		
		String[] nombre = {"Ruben","Celia","Noelia","Oscar","Lucia","David","Miguel","Nerea"};
		Integer[] edad = {32, 20, 65, 25, 55, 44, 30, 22};
		
		File fichero = null;
		FileOutputStream fileout = null;
		FileInputStream filein = null;
		DataOutputStream filedos = null;
		
		try {
			
			fichero = new File ("FicheroDatos.txt");
			fileout = new FileOutputStream (fichero);
			filein = new FileInputStream(fichero);
			filedos = new DataOutputStream(fileout);
			
			for (int i=1; i<nombre.length; i++){
				filedos.writeUTF(nombre[i] + " ");
				filedos.writeInt(edad[i]);
				System.out.println("  ");
			}
			
			fileout.close();
			filedos.close();
					
		} catch (IOException e) {
			
			e.printStackTrace();
			
		}finally {
			
			System.out.println("Programa finalizado.");
			
		}
		
	}

}
