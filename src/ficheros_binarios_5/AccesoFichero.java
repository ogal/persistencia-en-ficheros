package ficheros_binarios_5;

import java.io.FileReader;
import java.io.IOException;
/**
 * Programa de testeo
 * */
public class AccesoFichero {

	public static void main(String[] args) {
		
		Leer_fichero uno = new Leer_fichero();
		
		uno.lee();
		
	}
}

class Leer_fichero{
	
	public void lee() {
		
		try {
			FileReader entrada = new FileReader("C:/Users/oscar/eclipse-workspace/m6/mifichero.java");
			
			int c = 0;
			
			while(c != -1) {
				
				c = entrada.read();//esto nos devuelve el valor unicode del caracter
				
				char letra = (char)c;//Parseamos el valor unicode a un valor de tipo caracter.
				
				System.out.print(letra);//vemos que al final sale '?', esto corresponde al valor unicode -1
			}
						
			entrada.close();
			
		} catch (IOException e) {

			e.printStackTrace();
		}
		
	}
}