package ficheros_binarios_5;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
/**
 * Programa de testeo creando una memoria intermedia
 * */
public class LecturaBuffer {

	public static void main(String[] args) {
		
		LeerFichero tres = new LeerFichero();
		
		tres.lee();
		
	}
}

class LeerFichero{
	
	public void lee() {
		
		try {
			FileReader entrada = new FileReader("C:/Users/oscar/eclipse-workspace/m6/ejemplo3.txt");
			
			BufferedReader miBuffer = new BufferedReader(entrada);
			
			String linea = "";
			
			while(linea != null) {
				
				linea = miBuffer.readLine();//esto lee cada una de las l�neas, hasta \n o \r (salto de l�nea o enter)
				
				if(linea != null) {//con esto evitamos que salga el �ltimo null de haber llegado al final del fichero
					System.out.println(linea);
				}
					
			}
						
			miBuffer.close();
			entrada.close();
			
			
		} catch (IOException e) {

			System.out.println();
		}
		
	}
}
