package flujos_streams_3;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

/**
 * Programa que va leyendo caracteres de 20 en 20.
 * Pasar el nombre del fichero al programa.
 * */
public class LeerFicheroTexto {
	
	static Scanner teclado = new Scanner(System.in);
	
	public static void main ( String [] args) throws IOException {
			
		File fichero = new File ("LeerFicheroTexto.java"); // declaraci�n fichero
		FileReader lector = new FileReader (fichero); // creamos flujo de entrada hacia el fichero
		int i;
		int contador = 0; 
		
		while ((i = lector.read()) != -1) { //Vamos leyendo car�cter a car�cter
			
			System.out.print ((char) i); //hacemos cast a char del entero le�do
			
			contador++; 
			
			if ( contador%20 == 0 ) {
				
				System.out.println("\n");
			}
			
		}
		
		lector.close();
		
	}

}
