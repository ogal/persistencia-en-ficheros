package flujos_streams_3;

import javax.swing.JFileChooser;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

/** OK - PDF3 -> PROBLEMAS STREAMS 3
 * */
public class MuestraContenido {
	
//	NUMERO 1
	public static void leeElArchivo(File archivo) {
		
//		Escogemos el archivo, buscador situado en la carpeta actual
		JFileChooser pick = new JFileChooser(".");
		pick.showOpenDialog(pick);
		
		archivo = new File(pick.getSelectedFile().getAbsolutePath());
		FileReader lector = null;
		BufferedReader buffer = null;
		
        try {
        	
			lector = new FileReader(archivo);
			buffer = new BufferedReader(lector);
			
			while (buffer.ready()) {
				System.out.println(buffer.readLine());
			}
			
			buffer.close();
			lector.close();
			
			System.out.println("\n Final del documento.");
			
		} catch (IOException e) {
			
			System.out.println("Lo sentimos, el archivo no ha sido encontrado.");
			
		} 
	}
	
//	NUMERO 2
	
	
//	NUMERO 3
	public static void escribeDiezFilasDeCarac() {
		
		String linea = "Linea numero: ";
		
		File archivo2 = new File("diezLineas.txt");
		PrintWriter escribe = null;
		BufferedWriter bufer = null;
		
		try {
			
			escribe = new PrintWriter(archivo2);
			bufer = new BufferedWriter(escribe);
        	
			if(!archivo2.exists()) {
				archivo2.createNewFile();
			}
			
			for(int i = 0; i < 10; i++) {
				
				bufer.write(linea + " - " + ( i+1 ));
				bufer.newLine();
				
			}
			
			bufer.close();
			escribe.close();
			
		} catch (IOException e) {
			
			System.out.println("Lo sentimos, el archivo no ha sido encontrado.");
			
		} 
	}

	public static void main(String[] args) throws IOException{
		
//		NUMERO 1
		//leeElArchivo(new File(""));
		
//		NUMERO 2
		
		
//		NUMERO 3
		escribeDiezFilasDeCarac();
        
	}

}
