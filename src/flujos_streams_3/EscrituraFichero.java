package flujos_streams_3;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Modifica EscribirFicheroTexto.java para, en vez de escribir los caracteres uno a uno,
 * escribir todo el array usando el m�todo write (char [] buf)
 * */
public class EscrituraFichero {

	public static void main(String[] args) throws IOException {
		
		File fichero = new File("FicheroTexto.txt");
		FileWriter escritor = new FileWriter (fichero);
		String frase = "Esto es una segunda prueba con FileWriter y su metodo write()";
		
		char[] cadena = frase.toCharArray();
		
		for ( int i=0; i < cadena.length ; i++) {
			escritor.write (cadena, cadena.length, i);
		}
		
		escritor.close (); // cerramos fichero

	}

}
