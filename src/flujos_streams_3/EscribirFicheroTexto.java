package flujos_streams_3;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**Ejercicio del PDF numero 3, Problemas Strems 2
 * 1. Programa que escribe un array usando el m�todo write (char [] buf)
 * 2. Crea el siguiente array de String e inserta en el fichero las cadenas una a una
 * usando el m�todo write (String str)
 * */
public class EscribirFicheroTexto {
	
//	NUMERO 2 
	public static void insertaUnArrayDeChars(String frase) {
		
		File fichero = new File("FicheroTexto.txt");
		FileWriter escritor = null;
		
		try {
			
			char[] cadena = null;
			
			if(!fichero.exists()) {
				fichero.createNewFile();
			}
			escritor = new FileWriter (fichero);
			cadena = frase.toCharArray();
			
//			for ( int i=0; i < cadena.length ; i++) {
//				escritor.write (cadena[i]); // se va escribiendo car�cter a car�cter
//			}
			
//			Escribimos la cadena de caracteres de golpe
			escritor.write(cadena);
			
			escritor.append ("***"); // a�adimos un asterisco al final
			escritor.close (); // cerramos fichero
			
		} catch (IOException e) {
			
			System.out.println("Ha habido un error: " + e);
		}
		
	}
	
//	NUMERO 3
	public static void insertaUnArrayDeStrings(String[] provincias) {
		
		File nombres = new File("provincias.txt");
		FileWriter escribe = null;
		BufferedWriter bufer = null;
		
		try {
				
			if(!nombres.exists()) {
				nombres.createNewFile();
			}
			
			escribe = new FileWriter(nombres);
			bufer = new BufferedWriter(escribe);
			
			for(String provincia : provincias) {
				
				bufer.write(provincia + "\n");			
			}
			
			bufer.close();
			escribe.close();
			
		} catch (IOException e) {
				
			System.out.println("No se ha creado el archivo");
		}
	}

	public static void main(String[] args) throws IOException {
			
//		NUMERO 2
		String frase = "Esto es una prueba con FileWriter y el bufer de char";
		
		insertaUnArrayDeChars(frase);
		
//		NUMERO 3 
		String prov[] = {"Albacete", "Avila", "Badajoz", "Caceres", "Huelva", "Jaen",
						"Madrid", "Segovia", "Soria", "Toledo", "Valladolid", "Zamora"};
		
		insertaUnArrayDeStrings(prov);
		
	}
}
